stages:
    - build
    - test
    - deploy
build:
    stage: build
    tags: 
        - gitlab
    script:
        - echo "Hello_Denis!"

test:
   stage: test
   tags: 
       - gitlab
   script:
       - echo "Delete file"
             
dev:
    stage: deploy
    tags: 
        - gitlab
    script:
        - echo "Dev"
    when: manual
    

prod:
    stage: deploy
    tags: 
        - gitlab
    script:
        - echo "Prod"
    when: manual
